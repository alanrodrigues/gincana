package com.example.alanm.gincanadascores.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alanm.gincanadascores.R;
import com.example.alanm.gincanadascores.persistence.entities.Gincana;

import java.util.ArrayList;
import java.util.List;

public class GincanaAdapter extends RecyclerView.Adapter<GincanaAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener{


    List<Gincana> gincanas;

    public GincanaAdapter(List<Gincana> gincanas) {
        this.gincanas = gincanas;
    }



    @NonNull
    @Override
    public GincanaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.design_gincana, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GincanaAdapter.ViewHolder viewHolder, int i) {
        viewHolder._nome.setText(gincanas.get(i).getNome());
        viewHolder._total.setText(String.valueOf(gincanas.get(i).getEquipes()));


    }

    @Override
    public int getItemCount() {
        return gincanas.size();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView _nome;
        TextView _total;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            _nome = itemView.findViewById(R.id.txt_nome_gincana);
            _total = itemView.findViewById(R.id.txt_total);


        }
    }
}
