package com.example.alanm.gincanadascores.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alanm.gincanadascores.R;
import com.example.alanm.gincanadascores.persistence.entities.Prova;

import java.util.List;

public class ProvasAdapter extends RecyclerView.Adapter<ProvasAdapter.ViewHolder> {

    List<Prova> provaList;


    public ProvasAdapter(List<Prova> provaList) {
        this.provaList = provaList;
    }

    @NonNull
    @Override
    public ProvasAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.design_prova, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProvasAdapter.ViewHolder viewHolder, int i) {
        viewHolder._nome_prova.setText(provaList.get(i).getNome());
        viewHolder._qtd_pontos.setText(String.valueOf(provaList.get(i).getPontos_total()));
    }

    @Override
    public int getItemCount() {
        return provaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
         TextView _nome_prova;
         TextView _qtd_pontos;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            _nome_prova = itemView.findViewById(R.id.txt_nome_prova);
            _qtd_pontos = itemView.findViewById(R.id.txt_qtd_pontos);

        }
    }
}
