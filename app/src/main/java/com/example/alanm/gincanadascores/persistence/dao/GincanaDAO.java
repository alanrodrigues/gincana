package com.example.alanm.gincanadascores.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.alanm.gincanadascores.persistence.entities.Gincana;

import java.util.List;

@Dao
public interface GincanaDAO {
    @Query("SELECT * FROM gincanas")
    List<Gincana> getGincanas();

    @Insert
    void insertGincana(Gincana gincana);

    @Update
    void atualizarGincana(Gincana gincana);

    @Delete
    void apagarGincana(Gincana gincana);


}
