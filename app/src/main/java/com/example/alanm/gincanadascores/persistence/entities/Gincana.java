package com.example.alanm.gincanadascores.persistence.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "gincanas")
public class Gincana {

    public Gincana(String nome) {
        this.nome = nome;
    }

    public Gincana() {

    }

    @PrimaryKey (autoGenerate = true)
    private int id;

    @ColumnInfo(name = "nome_gincana")
    private String nome;

    @ColumnInfo(name = "total_equipes")
    private int equipes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getEquipes() {
        return equipes;
    }

    public void setEquipes(int equipes) {
        this.equipes = equipes;
    }
}
