package com.example.alanm.gincanadascores.ui.activies;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.alanm.gincanadascores.R;
import com.example.alanm.gincanadascores.persistence.DB;
import com.example.alanm.gincanadascores.persistence.Persistence;
import com.example.alanm.gincanadascores.persistence.entities.Gincana;

public class CadastrarGincanaActivity extends AppCompatActivity {

    private EditText _nome;
    private Button _btn;
    private Spinner _equipes;
    private ArrayAdapter adapter;

    int idGincana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_gincana);
        _nome = findViewById(R.id.nome_gincana);
        _btn = findViewById(R.id.btn);
        _equipes = findViewById(R.id.numero_equipes);

        adapter = ArrayAdapter.createFromResource(this, R.array.equipes_total, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _equipes.setAdapter(adapter);


        _btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                idGincana = getIntent().getExtras().getInt("idGincana", 0);
                validarCampos(_nome, _equipes);

            }
        });


    }


    public void validarCampos(EditText name, Spinner equipes){
        Gincana gincana = new Gincana();
        String nome = name.getText().toString();
        String item = equipes.getSelectedItem().toString();
        gincana.setNome(nome);
        gincana.setEquipes(Integer.parseInt(item));
        Persistence.getDb(this).gincanaDAO().insertGincana(gincana);
        startActivity(new Intent(CadastrarGincanaActivity.this, MainActivity.class));
    }
}
