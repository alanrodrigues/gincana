package com.example.alanm.gincanadascores.ui.activies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alanm.gincanadascores.R;
import com.example.alanm.gincanadascores.persistence.Persistence;
import com.example.alanm.gincanadascores.persistence.entities.Prova;

public class CadastrarProvasActivity extends AppCompatActivity {

    private EditText _nome_prova;
    private EditText _qtd_pontos;
    private Button _btnSalvarProva;
    int idGincana = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_provas);

        _nome_prova = findViewById(R.id.nome_prova);
        _qtd_pontos = findViewById(R.id.qtd_pontos);
        _btnSalvarProva = findViewById(R.id.btnSalvarProva);

        idGincana = getIntent().getExtras().getInt("idGincana", 0);


        _btnSalvarProva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verificaCampos(_nome_prova, _qtd_pontos);
            }
        });


    }

    public void verificaCampos(EditText nome, EditText pontos){
        String n = nome.getText().toString();
        String p = pontos.getText().toString();

        if(n.equals("") && p.equals("")){
            Toast.makeText(getApplicationContext(), "Preencha todos os campos", Toast.LENGTH_SHORT).show();
        }else {
            Prova prova = new Prova();
            prova.setNome(n);
            prova.setGincanaId(idGincana);
            prova.setPontos_total(Integer.parseInt(p));
            Persistence.getDb(getApplicationContext()).provaDAO().cadastrarProva(prova);
            Toast.makeText(getApplicationContext(), "Prova cadastrada com sucesso", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
