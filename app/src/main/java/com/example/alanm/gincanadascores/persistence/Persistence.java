package com.example.alanm.gincanadascores.persistence;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;

public class Persistence  {
    private static DB db;
    private static SharedPreferences preferences;

    public static DB getDb(Context context){
        if(db == null){
             db = Room.databaseBuilder(context, DB.class, "banco").allowMainThreadQueries().build();
        }
        return db;
    }

    public static SharedPreferences getPreferences(Context context){
        if (preferences == null){
            preferences = context.getSharedPreferences("preferences", context.MODE_PRIVATE);
        }
        return preferences;
    }
}
