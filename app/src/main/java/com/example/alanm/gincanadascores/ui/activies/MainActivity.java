package com.example.alanm.gincanadascores.ui.activies;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.alanm.gincanadascores.R;
import com.example.alanm.gincanadascores.persistence.Persistence;
import com.example.alanm.gincanadascores.persistence.entities.Gincana;
import com.example.alanm.gincanadascores.ui.adapters.GincanaAdapter;
import com.example.alanm.gincanadascores.ui.adapters.RecyclerItemClickListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private FloatingActionButton fab;
    private RecyclerView rc;
    private RecyclerView.Adapter adapter;
    GincanaAdapter gincanaAdapter;
    List<Gincana> gincanaList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        rc = findViewById(R.id.recycler);


        gincanaList = Persistence.getDb(this).gincanaDAO().getGincanas();


        rc.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GincanaAdapter(gincanaList);
        rc.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        rc.setAdapter(adapter);
        rc.setHasFixedSize(true);

        rc.addOnItemTouchListener(new RecyclerItemClickListener(this, rc, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            Intent intent = new Intent(MainActivity.this, ProvasActivity.class);
            intent.putExtra("idGincana", gincanaList.get(position).getId());
            startActivity(intent);

            }

            @Override
            public void onLongItemClick(View view, int position) {

            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        }));


        fab = findViewById(R.id.btn_cadastrar);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CadastrarGincanaActivity.class));
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        gincanaAdapter = new GincanaAdapter(gincanaList);

    }


}
