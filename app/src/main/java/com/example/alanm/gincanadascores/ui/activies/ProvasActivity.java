package com.example.alanm.gincanadascores.ui.activies;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.example.alanm.gincanadascores.R;
import com.example.alanm.gincanadascores.persistence.Persistence;
import com.example.alanm.gincanadascores.persistence.entities.Gincana;
import com.example.alanm.gincanadascores.persistence.entities.Prova;
import com.example.alanm.gincanadascores.ui.adapters.ProvasAdapter;

import java.util.List;

public class ProvasActivity extends AppCompatActivity {

    private FloatingActionButton fab;
    private RecyclerView recyclerView;
    List<Prova> provas;
    ProvasAdapter provasAdapter;
    int idGincana;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fab = findViewById(R.id.fab);
        recyclerView = findViewById(R.id.recycler_prova);

        idGincana = getIntent().getExtras().getInt("idGincana", 0);



        provas = Persistence.getDb(this).provaDAO().getProvas(idGincana);

        recyclerView.setLayoutManager( new LinearLayoutManager(this));
        provasAdapter = new ProvasAdapter(provas);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent it =  new Intent(ProvasActivity.this, CadastrarProvasActivity.class);
                it.putExtra("idGincana",idGincana);
               startActivity(it);

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setAdapter(provasAdapter);
    }
}
