package com.example.alanm.gincanadascores.persistence;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomDatabase;
import android.support.annotation.NonNull;

import com.example.alanm.gincanadascores.persistence.dao.GincanaDAO;
import com.example.alanm.gincanadascores.persistence.dao.ProvaDAO;
import com.example.alanm.gincanadascores.persistence.entities.Gincana;
import com.example.alanm.gincanadascores.persistence.entities.Prova;

@Database(entities = {Gincana.class, Prova.class}, version = 1)
public abstract class DB extends RoomDatabase {
    public abstract GincanaDAO gincanaDAO();
    public abstract ProvaDAO provaDAO();
}
