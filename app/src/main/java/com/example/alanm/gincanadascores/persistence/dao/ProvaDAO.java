package com.example.alanm.gincanadascores.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.alanm.gincanadascores.persistence.entities.Prova;

import java.util.List;

@Dao
public interface ProvaDAO {

    @Insert
    void cadastrarProva(Prova prova);

    @Query("SELECT * FROM provas WHERE gincanaId = :id_gincana")
    List<Prova> getProvas(int id_gincana);

    @Query("SELECT * FROM provas")
    List<Prova> getProva();


}
