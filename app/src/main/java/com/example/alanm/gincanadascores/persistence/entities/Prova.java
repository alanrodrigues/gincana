package com.example.alanm.gincanadascores.persistence.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity (tableName = "provas",foreignKeys = @ForeignKey(entity = Gincana.class, parentColumns = "id", childColumns = "gincanaId", onDelete = CASCADE))
public class Prova {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo (name = "nome_prova")
    private String nome;

    @ColumnInfo (name = "gincanaId")
    private int gincanaId;

    @ColumnInfo(name = "quantidade_pontos")
    private int pontos_total;

    public int getPontos_total() {
        return pontos_total;
    }

    public void setPontos_total(int pontos_total) {
        this.pontos_total = pontos_total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getGincanaId() {
        return gincanaId;
    }

    public void setGincanaId(int gincanaId) {
        this.gincanaId = gincanaId;
    }
}
